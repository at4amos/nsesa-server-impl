<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">

    <modelVersion>4.0.0</modelVersion>
    <groupId>org.nsesa</groupId>
    <artifactId>server-demo-impl</artifactId>
    <packaging>war</packaging>
    <name>Server Impl</name>
    <version>0.5</version>

    <repositories>
        <repository>
            <id>maven</id>
            <url>http://repo2.maven.org/maven2</url>
        </repository>
        <repository>
            <id>java.net</id>
            <url>http://download.java.net/maven/2</url>
        </repository>
        <repository>
            <id>codehaus.org</id>
            <url>http://repository.codehaus.org</url>
        </repository>
    </repositories>

    <properties>
        <gwt.version>2.5.0</gwt.version>
        <spring.version>3.1.0.RELEASE</spring.version>
        <gwtplugin.version>2.5.0</gwtplugin.version>
        <querydsl.version>2.9.0</querydsl.version>
        <hibernate.version>4.1.10.Final</hibernate.version>
        <jdk.source>1.6</jdk.source>
        <jdk.target>1.6</jdk.target>

        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <project.reporting.outputEncoding>UTF-8</project.reporting.outputEncoding>
        <project.name.long>Demo backend implementation</project.name.long>
        <maven.build.timestamp.format>yyyyMMdd_HHmm</maven.build.timestamp.format>
        <build.timestamp>${maven.build.timestamp}</build.timestamp>
    </properties>

    <dependencies>
        <!-- AN editor -->
        <dependency>
            <groupId>org.nsesa</groupId>
            <artifactId>nsesa-server-api</artifactId>
            <version>0.5</version>
        </dependency>

        <!--Spring-->
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-core</artifactId>
            <version>${spring.version}</version>
        </dependency>
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-webmvc</artifactId>
            <version>${spring.version}</version>
        </dependency>
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-orm</artifactId>
            <version>${spring.version}</version>
        </dependency>
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-tx</artifactId>
            <version>${spring.version}</version>
        </dependency>
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-aspects</artifactId>
            <version>${spring.version}</version>
        </dependency>
        <dependency>
            <groupId>org.springframework.data</groupId>
            <artifactId>spring-data-jpa</artifactId>
            <version>1.3.0.RELEASE</version>
        </dependency>
        <dependency>
            <groupId>org.aspectj</groupId>
            <artifactId>aspectjrt</artifactId>
            <version>1.7.2</version>
            <scope>runtime</scope>
        </dependency>
        <dependency>
            <groupId>org.aspectj</groupId>
            <artifactId>aspectjweaver</artifactId>
            <version>1.7.2</version>
            <scope>runtime</scope>
        </dependency>

        <!-- In memory DB -->
        <dependency>
            <groupId>com.h2database</groupId>
            <artifactId>h2</artifactId>
            <version>1.3.170</version>
        </dependency>

        <!-- Postgresql -->
        <dependency>
            <groupId>postgresql</groupId>
            <artifactId>postgresql</artifactId>
            <version>9.1-901.jdbc4</version>
        </dependency>

        <!-- MySQL -->
        <dependency>
            <groupId>mysql</groupId>
            <artifactId>mysql-connector-java</artifactId>
            <version>5.1.24</version>
        </dependency>

        <dependency>
            <groupId>com.mchange</groupId>
            <artifactId>c3p0</artifactId>
            <version>0.9.5-pre1</version>
        </dependency>

        <dependency>
            <groupId>org.codehaus.jackson</groupId>
            <artifactId>jackson-core-asl</artifactId>
            <version>1.9.7</version>
        </dependency>
        <dependency>
            <groupId>org.codehaus.jackson</groupId>
            <artifactId>jackson-mapper-asl</artifactId>
            <version>1.9.7</version>
        </dependency>
        <dependency>
            <groupId>org.codehaus.jackson</groupId>
            <artifactId>jackson-xc</artifactId>
            <version>1.9.7</version>
        </dependency>
        <dependency>
            <groupId>org.codehaus.jackson</groupId>
            <artifactId>jackson-jaxrs</artifactId>
            <version>1.9.7</version>
        </dependency>

        <dependency>
            <groupId>javax.servlet</groupId>
            <artifactId>jstl</artifactId>
            <version>1.2</version>
            <scope>runtime</scope>
        </dependency>

        <!-- core module: use can you it on its own if you are not using Spring 3 -->
        <dependency>
            <groupId>com.inspire-software.lib.dto.geda</groupId>
            <artifactId>geda.core</artifactId>
            <version>3.0.0</version>
        </dependency>

        <!-- spring integration: spring 3 AOP wrapper for core -->
        <dependency>
            <groupId>com.inspire-software.lib.dto.geda</groupId>
            <artifactId>geda.spring-integration</artifactId>
            <version>3.0.0</version>
        </dependency>

        <dependency>
            <groupId>commons-lang</groupId>
            <artifactId>commons-lang</artifactId>
            <version>2.6</version>
        </dependency>
        <dependency>
            <groupId>commons-logging</groupId>
            <artifactId>commons-logging</artifactId>
            <version>1.1.1</version>
            <scope>runtime</scope>
        </dependency>
        <dependency>
            <groupId>org.slf4j</groupId>
            <artifactId>slf4j-log4j12</artifactId>
            <version>1.6.1</version>
        </dependency>

        <!-- CXF -->
        <dependency>
            <groupId>org.apache.cxf</groupId>
            <artifactId>cxf-bundle-minimal</artifactId>
            <version>2.7.3</version>

            <exclusions>
                <exclusion>
                    <groupId>org.apache.geronimo.specs</groupId>
                    <artifactId>geronimo-javamail_1.4_spec</artifactId>
                </exclusion>
                <exclusion>
                    <groupId>org.apache.geronimo.specs</groupId>
                    <artifactId>geronimo-activation_1.1_spec</artifactId>
                </exclusion>
                <exclusion>
                    <groupId>org.apache.geronimo.specs</groupId>
                    <artifactId>geronimo-servlet_3.0_spec</artifactId>
                </exclusion>
                <exclusion>
                    <groupId>org.apache.geronimo.specs</groupId>
                    <artifactId>geronimo-annotation_1.0_spec</artifactId>
                </exclusion>
                <exclusion>
                    <groupId>org.apache.geronimo.specs</groupId>
                    <artifactId>geronimo-jaxws_2.2_spec</artifactId>
                </exclusion>
                <exclusion>
                    <groupId>org.apache.geronimo.specs</groupId>
                    <artifactId>geronimo-jms_1.1_spec</artifactId>
                </exclusion>
                <exclusion>
                    <groupId>org.apache.geronimo.specs</groupId>
                    <artifactId>geronimo-stax-api_1.0_spec</artifactId>
                </exclusion>
                <exclusion>
                    <groupId>org.apache.geronimo.specs</groupId>
                    <artifactId>geronimo-ws-metadata_2.0_spec</artifactId>
                </exclusion>
                <exclusion>
                    <groupId>org.eclipse.jetty</groupId>
                    <artifactId>jetty-http</artifactId>
                </exclusion>
                <exclusion>
                    <groupId>org.eclipse.jetty</groupId>
                    <artifactId>jetty-continuation</artifactId>
                </exclusion>
                <exclusion>
                    <groupId>org.eclipse.jetty</groupId>
                    <artifactId>jetty-io</artifactId>
                </exclusion>
                <exclusion>
                    <groupId>org.eclipse.jetty</groupId>
                    <artifactId>jetty-security</artifactId>
                </exclusion>
                <exclusion>
                    <groupId>org.eclipse.jetty</groupId>
                    <artifactId>jetty-server</artifactId>
                </exclusion>
            </exclusions>
        </dependency>

        <!-- Hibernate & JPA-->
        <dependency>
            <groupId>org.hibernate.javax.persistence</groupId>
            <artifactId>hibernate-jpa-2.0-api</artifactId>
            <version>1.0.1.Final</version>
        </dependency>
        <dependency>
            <groupId>org.hibernate</groupId>
            <artifactId>hibernate-entitymanager</artifactId>
            <version>${hibernate.version}</version>
            <scope>runtime</scope>
        </dependency>

        <dependency>
            <groupId>joda-time</groupId>
            <artifactId>joda-time</artifactId>
            <version>1.6</version>
        </dependency>
        <!-- Freemarker templates (to be used with the generation of overlay elements -->
        <dependency>
            <groupId>org.freemarker</groupId>
            <artifactId>freemarker</artifactId>
            <version>2.3.19</version>
        </dependency>
        <!-- Xerces for (somewhat) sane XML validation -->
        <dependency>
            <groupId>xerces</groupId>
            <artifactId>xercesImpl</artifactId>
            <version>2.10.0</version>
        </dependency>


        <!-- TEST ONLY DEPENDENCIES -->
        <dependency>
            <groupId>junit</groupId>
            <artifactId>junit</artifactId>
            <version>4.5</version>
            <scope>test</scope>
        </dependency>

        <dependency>
            <groupId>org.easymock</groupId>
            <artifactId>easymock</artifactId>
            <version>3.1</version>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.easymock</groupId>
            <artifactId>easymockclassextension</artifactId>
            <version>3.1</version>
            <scope>test</scope>
        </dependency>
    </dependencies>

    <build>
        <finalName>services</finalName>
        <resources>
            <resource>
                <directory>src/main/resources</directory>
                <filtering>true</filtering>
            </resource>

        </resources>
        <plugins>
            <!-- ***************************** Configure compiler plugin    ***************************** -->
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-compiler-plugin</artifactId>
                <version>2.3.2</version>
                <configuration>
                    <source>${jdk.source}</source>
                    <target>${jdk.target}</target>
                    <encoding>UTF-8</encoding>
                </configuration>
                <dependencies>
                    <dependency>
                        <groupId>org.codehaus.plexus</groupId>
                        <artifactId>plexus-compiler-javac</artifactId>
                        <version>1.8.6</version>
                    </dependency>
                </dependencies>
            </plugin>
            <!-- ***************************** Configure WAR plugin    ***************************** -->
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-war-plugin</artifactId>
                <version>2.1.1</version>
                <configuration>
                    <warSourceDirectory>web</warSourceDirectory>
                    <webResources>
                        <resource>
                            <!-- this is relative to the pom.xml directory -->
                            <directory>${basedir}/src/env/${env}</directory>
                            <filtering>true</filtering>
                        </resource>
                    </webResources>
                </configuration>
            </plugin>

            <!-- ***************************** Jetty plugin    ***************************** -->
            <plugin>
                <groupId>org.mortbay.jetty</groupId>
                <artifactId>maven-jetty-plugin</artifactId>
                <version>6.1.26</version>
                <configuration>
                    <webAppSourceDirectory>web</webAppSourceDirectory>
                    <contextPath>/services</contextPath>
                    <scanIntervalSeconds>10</scanIntervalSeconds>
                    <connectors>
                        <connector implementation="org.mortbay.jetty.nio.SelectChannelConnector">
                            <port>9090</port>
                            <maxIdleTime>60000</maxIdleTime>
                        </connector>
                    </connectors>
                    <stopKey>NsesaServer</stopKey>
                    <stopPort>9813</stopPort>
                </configuration>
            </plugin>
        </plugins>
    </build>

    <!-- ***************************** Profiles ***************************** -->
    <profiles>
        <profile>
            <!-- local development profile -->
            <id>local</id>
            <activation>
                <activeByDefault>true</activeByDefault>
                <property>
                    <name>local</name>
                </property>
            </activation>
            <properties>
                <env>local</env>
            </properties>
        </profile>
        <profile>
            <!-- Development env. profile -->
            <id>dev</id>
            <activation>
                <property>
                    <name>dev</name>
                </property>
            </activation>
            <properties>
                <env>dev</env>
            </properties>
        </profile>
        <profile>
            <id>querydsl</id>
            <activation>
                <activeByDefault>true</activeByDefault>
            </activation>
            <dependencies>

                <dependency>
                    <groupId>com.mysema.querydsl</groupId>
                    <artifactId>querydsl-jpa</artifactId>
                    <version>${querydsl.version}</version>
                </dependency>

            </dependencies>

            <build>
                <plugins>
                    <plugin>
                        <groupId>com.mysema.maven</groupId>
                        <artifactId>maven-apt-plugin</artifactId>
                        <version>1.0.4</version>
                        <dependencies>
                            <dependency>
                                <groupId>com.mysema.querydsl</groupId>
                                <artifactId>querydsl-apt</artifactId>
                                <version>${querydsl.version}</version>
                            </dependency>
                        </dependencies>
                        <executions>
                            <execution>
                                <phase>generate-sources</phase>
                                <goals>
                                    <goal>process</goal>
                                </goals>
                                <configuration>
                                    <outputDirectory>target/generated-sources/queries</outputDirectory>
                                    <processor>com.mysema.query.apt.jpa.JPAAnnotationProcessor</processor>
                                </configuration>
                            </execution>
                        </executions>
                    </plugin>
                </plugins>
            </build>
        </profile>
    </profiles>
</project>
